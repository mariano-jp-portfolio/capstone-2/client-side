// Admin - Add Course

let formSubmit = document.querySelector('#createCourse');

// Add an event listener to add course
formSubmit.addEventListener('submit', (e) => {
	e.preventDefault();
	
	// Get the values for Course object
	let courseName = document.querySelector('#courseName').value;
	let description = document.querySelector('#courseDescription').value;
	let price = document.querySelector('#coursePrice').value;
	
	// Retrieve the token & proceed to add a course
	let token = localStorage.getItem('token');
	
	if (courseName == "" || description == "" || price == "") {
		alert('Please enter stuff below to create a course.');
	} else {
		fetch('https://jp-course-booking-app.herokuapp.com/api/courses', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => {
				return res.json();
		})
		.then(data => {
			// Create a new course if successful
			if (data === true) {
				// redirect the user to the course page
				alert('Course successfully created!');
				window.location.replace("./courses.html");
			} else {
				// error in creating a new course
				alert('Oops, something went wrong.');
			}
		});
	}
});