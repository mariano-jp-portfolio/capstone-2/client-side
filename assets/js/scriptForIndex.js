// Add the logic that will show the register button in the index page
let register = document.querySelector('#registrationLink');
let login = document.querySelector('#loginLink');
let course = document.querySelector('#courseLink');

let userToken = localStorage.getItem("token");
// console.log(userToken);

if (!userToken) {
	login.innerHTML =
	`
		<li class="nav-item">
			<a href="./pages/login.html" class="nav-link"> Log In </a>
		</li>
	`;
	
	register.innerHTML =
	`
		<li class="nav-item">
			<a href="./pages/register.html" class="nav-link"> Sign Up </a>
		</li>
	`;
} else {
	course.innerHTML =
	`
		<li class="nav-item">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>
	`;
	
	login.innerHTML =
	`
		<li class="nav-item">
			<a href="./pages/userProfile.html" class="nav-link"> My Profile </a>
		</li>
	`;
	
	register.innerHTML =
	`		
		<li class="nav-item">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>
	`;
}