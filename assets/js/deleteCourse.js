// Admin - Archive (Soft Delete)

// create a variable for our course ID parameter
let params = new URLSearchParams(window.location.search);

// stores the retrieved variable
let courseId = params.get('courseId');

// token
let token = localStorage.getItem('token');

// Confirm deletion
if (window.confirm('Do you really want to archive this course?')) {
	// Fetch request to soft delete
	fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/${courseId}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => {
		return res.json();
	})
	.then(data => {
		// If successful, proceed with soft deletion
		if (data === true) {
			let archiveHeader = document.querySelector('#archiveHeader');
			let backBtn = document.querySelector('#backBtn');
		
			archiveHeader.innerHTML = 
			`
				<h2 id="archiveHeader" class="my-5"></span>Course successfully sent to archive.</h2>
			`;
		
			backBtn.innerHTML = 
			`
				<a href="./courses.html" class="btn btn-primary">Go Back to the Courses Page</a>
			`;
		} else {
			alert('Oops, something went wrong.');
		}
	});
} else {
	location.replace('./courses.html');
}