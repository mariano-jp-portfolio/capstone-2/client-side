// USER REGISTRATION

// Add an event to the register form
let registerForm = document.querySelector("#registerUser");
// console.log(registerForm);


// Add an event listener that will trigger when the user submits the form
registerForm.addEventListener('submit', (e) =>{
	//prevents page redirection to avoid loss of input data whenever the registration process is not successful
	e.preventDefault();

	//select all the input fields by adding query selector 
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	//validation to enable submit button when all fields are populated and both passwords match. and mobile no is equal to 11
	if ((password1 !== '' && password2 !== '' ) && (password2 === password1) && (mobileNo.length === 11)){

		// Check for duplicate email in database first
		fetch('https://jp-course-booking-app.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		

		// If no duplicates found, proceed
		if (data === false) {
			fetch('https://jp-course-booking-app.herokuapp.com/api/users/', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNo
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if(data === true) {
					//Registration successful
					alert("Registered successfully!");
					//redirect to login
					window.location.replace("./login.html");
				} else {
					alert("Oops, something went wrong.");
				}
			})
			.catch((error) => {
			  console.error('Error:', error);
			});
		}
		});

	} else {
		alert("Please check your password or make sure to enter 11-digit mobile number.");
	}
});