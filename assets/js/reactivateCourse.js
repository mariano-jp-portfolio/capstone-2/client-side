// Admin - Reactivate a course

// create a variable for our course ID parameter
let params = new URLSearchParams(window.location.search);

// stores the retrieved variable
let courseId = params.get('courseId');

// token
let token = localStorage.getItem('token');

// Confirm reactivation
if (window.confirm('Ready to publish this course again?')) {
	// Fetch request to reactivate
	fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/reactivate/${courseId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => {
		return res.json();
	})
	.then(data => {
		// If successful, proceed with reactivation
		if (data === true) {
			let reactivateHeader = document.querySelector('#reactivateHeader');
			let backBtn = document.querySelector('#backBtn');
			reactivateHeader.innerHTML = 
			`
				<h2 id="reactivateHeader" class="my-5"></span>Course reactivated successfully!</h2>
			`;
			
			backBtn.innerHTML = 
			`
				<a href="./courses.html" class="btn btn-primary">Go Back to the Courses Page</a>
			`;
		} else {
			alert('Oops, something went wrong.');
		}
	});
} else {
	location.replace('./courses.html');
}