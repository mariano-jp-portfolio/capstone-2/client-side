// Checks if the user is an Admin or not. If they are, give them Admin credentials

// This variable stores the isAdmin object in the user's local storage
let adminUser = localStorage.getItem("isAdmin");

// Cardfooter dynamically renders if the user is an admin or not
let cardFooter;

// courseData will store the data to be rendered
let courseData;

// renders all inactive courses for the Admin
let inactiveCourse;

// container for all courses
let container = document.querySelector('#coursesContainer');

// Combined queries for regular user or Admin access
// Fetch request to all users
fetch('https://jp-course-booking-app.herokuapp.com/api/courses') //getAll in Routes
.then(res => res.json())
.then(data => {
	// Logic
	if (data.length < 1) {
		courseData = "No courses available."
	} else {
		if (adminUser === "false" || !adminUser) {
			courseData = data.map(course => {
				cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block editButton">
							Select Course
						</a>
				`;
				
				return (
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">
										${course.name}
									</h5>
									
									<p class="card-text text-left">
										${course.description}
									</p>
									
									<p class="card-text text-right">
										${course.price}
									</p>
								</div>
								
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)
				
			}).join(""); // Our data collection is an array that is why we use join with separator "" to return a new string
			
			// To pass all active courses to our container (for regular user)
			container.innerHTML = courseData;
			
		} else {
			
			// Active courses
			courseData = data.map(course => {
				cardFooter =
					`
						<a href="./viewEnrollees.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block viewButton">
							View Enrollees
						</a>
						
						<a href="./editCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-warning text-white btn-block editButton">
							Edit
						</a>
					
						<a href="./deleteCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-danger text-white btn-block dangerButton">
							Delete
						</a>
				`;
				
				return (
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">
										${course.name}
									</h5>
									
									<p class="card-text text-left">
										${course.description}
									</p>
									
									<p class="card-text text-right">
										${course.price}
									</p>
								</div>
								
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)
				
			}).join("");
			
			// Fetch request for inactive course
			fetch('https://jp-course-booking-app.herokuapp.com/api/courses/inactive')
			.then(res => res.json())
			.then(data => {
				inactiveCourse = data.map(course => {
				cardFooter =
					`
						<a href="./editCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-warning text-white btn-block editButton">
							Edit
						</a>
					
						<a href="./reactivateCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-success text-white btn-block dangerButton">
							Reactivate
						</a>
				`;
			
				return (
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">
										${course.name}
									</h5>
									
									<p class="card-text text-left">
										${course.description}
									</p>
									
									<p class="card-text text-right">
										${course.price}
									</p>
								</div>
								
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)
			
			}).join("");
			
			// To pass all courses to our container (for Admin)
			container.innerHTML = courseData + inactiveCourse;
			});
		}
	}
});

// Repurposed modal to accomodate two buttons for the Admin and one for regular user
// If admin, options to add a course and view their profile page. If not, just to view their profile page
let modalButton = document.querySelector('#adminButton');

if (adminUser === "false" || !adminUser) {
	// modal button for regular user
	modalButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./userProfile" id="userProfile" class="btn btn-block btn-primary">
				My Profile
			</a>
		</div>
	`;
	
	let profileButton = document.querySelector('#userProfile');
	
	// Event listener
	profileButton.onclick = (e) => {
		e.preventDefault();
		window.location.replace('./userProfile.html');
	};
} else {
	// Admin's modal buttons
	modalButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse" id="addCourse" class="btn btn-block btn-success">
				Add Course
			</a>
		</div>
		
		<div class="col-md-2 offset-md-10">
			<a href="./userProfile" id="userProfile" class="btn btn-block btn-primary">
				My Profile
			</a>
		</div>
	`;
	
	let addCourse = document.querySelector('#addCourse');
	
	// Event listener
	addCourse.onclick = (e) => {
		e.preventDefault();
		window.location.replace('./addCourse.html');
	};
	
	let profileButton = document.querySelector('#userProfile');
	
	// Event listener
	profileButton.onclick = (e) => {
		e.preventDefault();
		window.location.replace('./userProfile.html');
	};
}