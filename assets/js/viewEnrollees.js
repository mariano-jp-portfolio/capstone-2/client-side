// Admin's view of all enrollees for a specific course

// create a variable for our course ID parameter
let params = new URLSearchParams(window.location.search);

// stores the retrieved variable
let courseId = params.get('courseId');

// token
let token = localStorage.getItem('token');

let enrollees = document.querySelector('#userEnrollments');
let backBtn = document.querySelector('#backContainer');
let enrolleesArr = [];

// Fetch all enrollees for a specific course
fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/enrollees/${courseId}`, {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	data.enrollees.map(enrolleeData => {
		fetch(`https://jp-course-booking-app.herokuapp.com/api/users/details/${enrolleeData.userId}`)
		.then(res => res.json())
		.then(data => {
			enrolleesArr.push(`${data.lastName}, ${data.firstName}`);
			let enrolleesSorted = enrolleesArr.sort();
			enrollees.innerHTML += 
			`
				<li>${enrolleesSorted.shift()}</li>
			`;
		});
	});
	
});

// Back button
backBtn.innerHTML =
		`
			<button id="backBtn" class="btn btn-block btn-primary">
				Back
			</button>
		`;
	
// Event listener for the Back button
document.querySelector('#backBtn').addEventListener('click', (e) => {
	e.preventDefault();
	window.location.replace('./courses.html');
});