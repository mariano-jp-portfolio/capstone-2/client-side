// USER LOGIN

// Add logic to the login form
let loginForm = document.querySelector('#logInUser');

// Add an event listener to the form
// When (e) is triggered, it will create an event object
loginForm.addEventListener("submit", (e) => {
	//prevents page redirection to avoid loss of input data whenever the registration process is not successful
	e.preventDefault();
	
	// Select all input fields
	let email = document.querySelector('#userEmail').value;
	let password = document.querySelector('#password').value;
	// console.log(email);
	// console.log(password);
	
	// Add logic that will generate an alert if email and password are blank, if not proceed to login
	if (email === "" || password === "") {
		alert(`Please input your email and/or password.`);
	} else {
		fetch('https://jp-course-booking-app.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json();
		}).then(data => {
			// Add logic that will store the token in the local storage, successful authentication will generate a JWT
			if (data.accessToken) {
				localStorage.setItem('token', data.accessToken);
				// Send fetch request to decode token and obtain user ID and role
				// Token URL's are private and should be hidden in our .env
				fetch('https://jp-course-booking-app.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				}).then(res => {
					return res.json();
				}).then(data => {
					// Set global user to have properties containing authenticated user's ID and role
					localStorage.setItem('id', data._id);
					localStorage.setItem('isAdmin', data.isAdmin);
					// If credentials are good, proceed to the next page
					window.location.replace('./courses.html');
				});
			} else {
				// Authentication Failure
				alert("Oops, something went wrong.");
			}
		});
	}
});