// User - Edit Profile

// create a variable for our course ID parameter
let params = new URLSearchParams(window.location.search);

// stores the retrieved variable
let userId = params.get('userId');

// token
let token = localStorage.getItem('token');

let formSubmit = document.querySelector('#updateUser');

// Add an event listener to update profile
formSubmit.addEventListener('submit', (e) => {
	e.preventDefault();
	
	// Get the values for updating the profile
	let firstName = document.querySelector('#firstName').value;
	let lastName = document.querySelector('#lastName').value;
	let mobileNo = document.querySelector('#mobileNumber').value;
	let email = document.querySelector('#userEmail').value;
	
	if (firstName == "" && lastName == "" && mobileNo == "" && email == "") {
		alert('Please enter stuff below to update your profile.');
	} else if (mobileNo == "" && email == "") {
		// To update the name, the user has to type in their first and last names
		if (firstName == "" || lastName == "") {
			alert('Please enter your full name to update your name.');
		} else {
			// Updating the user's first and last name
			fetch(`https://jp-course-booking-app.herokuapp.com/api/users/update/names`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName
				})
			})
			.then(res => {
				return res.json();
			})
			.then(data => {
				// Proceed with the changes if successful
				if (data === true) {
					alert('Your name has been updated.')
					// redirect back to their profile
					window.location.replace('./userProfile.html');
				} else {
					// error in updating their profile
					alert('Oops, something went wrong.');
				}
			});
		}
	} else if (firstName == "" && lastName == "" && mobileNo == "") {
		// Updating the user's email
		fetch(`https://jp-course-booking-app.herokuapp.com/api/users/update/email`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Your email has been updated.')
				// redirect back to their profile
				window.location.replace('./userProfile.html');
			} else {
				// error in updating their profile
				alert('Oops, something went wrong.');
			}
		});
	} else if (firstName == "" && lastName == "" && email == "") {
		// Updating the user's mobile number
		if (mobileNo.length !== 11) {
			alert(`11-digit mobile number is needed.`);
		} else {
			fetch(`https://jp-course-booking-app.herokuapp.com/api/users/update/num`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				mobileNo: mobileNo
			})
			})
			.then(res => {
				return res.json();
			})
			.then(data => {
				// Proceed with the changes if successful
				if (data === true) {
					alert('Your mobile number has been updated.')
					// redirect back to their profile
					window.location.replace('./userProfile.html');
				} else {
					// error in updating their profile
					alert('Oops, something went wrong.');
				}
			});
		}
	} else {
		// Updating all of the user's details
		fetch(`https://jp-course-booking-app.herokuapp.com/api/users/update`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Your profile has been updated.')
				// redirect back to their profile
				window.location.replace('./userProfile.html');
			} else {
				// error in updating their profile
				alert('Oops, something went wrong.');
			}
		});
	}
});