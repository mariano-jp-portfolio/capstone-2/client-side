// Admin - Edit Course

// create a variable for our course ID parameter
let params = new URLSearchParams(window.location.search);

// stores the retrieved variable
let courseId = params.get('courseId');

// token
let token = localStorage.getItem('token');

let formSubmit = document.querySelector('#editCourse');

// Add an event listener to edit course
formSubmit.addEventListener('submit', (e) => {
	e.preventDefault();
	
	// Get the values for Course editing
	let courseName = document.querySelector('#courseName').value;
	let description = document.querySelector('#courseDescription').value;
	let price = document.querySelector('#coursePrice').value;
	
	if (courseName == "" && description == "" && price == "") {
		alert('Please enter stuff below to edit this course.');
	} else if (description == "" && price == "") {
		// Editing the name only
		fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/edit/name/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Course name edited successfully!');
				// redirect to the courses page
				window.location.replace('./courses.html');
			} else {
				// error in editing a course
				alert('Oops, something went wrong.');
			}
		});
	} else if (courseName == "" && price == "") {
		// Editing the description only
		fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/edit/desc/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				description: description
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Course description edited successfully!');
				// redirect to the courses page
				window.location.replace('./courses.html');
			} else {
				// error in editing a course
				alert('Oops, something went wrong.');
			}
		});
	} else if (courseName == "" && description == "") {
		// Editing the price only
		fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/edit/price/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				price: price
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Course price edited successfully!');
				// redirect to the courses page
				window.location.replace('./courses.html');
			} else {
				// error in editing a course
				alert('Oops, something went wrong.');
			}
		});
	} else if (price == "") {
		// Editing the name and description only
		fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/edit/name_desc/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Course name & description edited successfully!');
				// redirect to the courses page
				window.location.replace('./courses.html');
			} else {
				// error in editing a course
				alert('Oops, something went wrong.');
			}
		});
	} else if (description == "") {
		// Editing the name and price only
		fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/edit/name_price/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				price: price
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Course name & price edited successfully!');
				// redirect to the courses page
				window.location.replace('./courses.html');
			} else {
				// error in editing a course
				alert('Oops, something went wrong.');
			}
		});
	} else if (courseName == "") {
		// Editing the description and price only
		fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/edit/desc_price/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				description: description,
				price: price
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Course description & price edited successfully!');
				// redirect to the courses page
				window.location.replace('./courses.html');
			} else {
				// error in editing a course
				alert('Oops, something went wrong.');
			}
		});
	} else {
		// Editing all details
		fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/edit/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => {
			return res.json();
		})
		.then(data => {
			// Proceed with the changes if successful
			if (data === true) {
				alert('Course edited successfully!');
				// redirect to the courses page
				window.location.replace('./courses.html');
			} else {
				// error in editing a course
				alert('Oops, something went wrong.');
			}
		});
	}
});