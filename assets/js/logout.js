// USER LOGOUT

// We need to clear the user's localStorage to let them logout
localStorage.clear();

// After clearing the cache, redirect to the login page
window.location.replace('./login.html');