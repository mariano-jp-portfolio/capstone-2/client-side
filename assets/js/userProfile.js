// User Profile Page

// This variable stores the isAdmin object in the user's local storage
let adminUser = localStorage.getItem("isAdmin");

// create a variable for our course ID parameter
let params = new URLSearchParams(window.location.search);

// stores the retrieved variable
let userId = params.get('userId');

// token
let token = localStorage.getItem('token');

let userName = document.querySelector('#userName');
let userEmail = document.querySelector('#userEmail');
let userMobile = document.querySelector('#userMobile');
let userEnrollments = document.querySelector('#userEnrollments');
let backBtn = document.querySelector('#backContainer');
let editBtn = document.querySelector('#editContainer');
let placeholderPara = document.querySelector('#placeholderPara');

// Fetch user's information
fetch('https://jp-course-booking-app.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	userName.innerHTML = `${data.firstName} ${data.lastName}`;
	userEmail.innerHTML = data.email;
	userMobile.innerHTML = 
		`
			<p id="userMobile" class="secondary">Mobile: ${data.mobileNo}</p>
		`;
	
	// Fetching the name of the course that the user is enrolled in and identifying if they're an admin or not
	// I used the route and controller from the course files instead of creating a new one in the user files
	if (adminUser === "false" || !adminUser) {
		data.enrollments.map(courseData => {
			fetch(`https://jp-course-booking-app.herokuapp.com/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {
				userEnrollments.innerHTML += 
					`
						<li>${data.name}</li>
					`;
			});
		});
	} else {
		// If the user is an admin, do nothing
		userEnrollments.innerHTML = null;
		placeholderPara.innerHTML = null;
	}
});

// Edit and back buttons
editBtn.innerHTML =
	`
		<button id="profileEditBtn" class="btn btn-block btn-warning">
			Update My Credentials
		</button>
	`;

// Event listener for the Edit button
document.querySelector('#profileEditBtn').addEventListener('click', (e) => {
	e.preventDefault();
	window.location.replace('./editProfile.html');
});

backBtn.innerHTML =
		`
			<button id="backBtn" class="btn btn-block btn-primary">
				Back to Courses
			</button>
		`;
	
// Event listener for the Back button
document.querySelector('#backBtn').addEventListener('click', (e) => {
	e.preventDefault();
	window.location.replace('./courses.html');
});