//Add the logic that will show the login button when a user is not logged in
let navItems = document.querySelector("#navSession");
let register = document.querySelector('#registerBtn');
// console.log(navItems);

let userToken = localStorage.getItem("token");
// console.log(userToken);

if (!userToken) {
	navItems.innerHTML = 
		`
			<li class="nav-item"> 
				<a href="./login.html" class="nav-link"> Log In </a>
			</li>
		`;
		
	register.innerHTML =
		`
			<li class="nav-item">
				<a href="./register.html" class="nav-link"> Sign Up </a>
			</li>
		`;
} else {
	navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./userProfile.html" class="nav-link"> My Profile </a>
			</li>		
		`;
		
	register.innerHTML =
		`
			<li class="nav-item"> 
				<a href="./logout.html" class="nav-link"> Log Out </a>
			</li>
		`;
}